﻿namespace JMD.Armery
{

    public class Damage 
    {

        public float damage;
        public float damageInTime;
        public float time;
        
        private DamageType damType = DamageType.BULLET;
        private float multiply = 1;

        public enum DamageType {
            BLUDGEON, SLASH, BULLET, POISON, DEATHLY, AREA
        }

        public Damage(DamageType weaponType, float multiplicador = 1){
            damType = weaponType;
            multiply = multiplicador;
            initDamage(weaponType);
            damage *= multiplicador;
            damageInTime *= multiplicador;
        }

        private void initDamage(DamageType dtype){
            // iniciamos variables
            damage = 0;
            damageInTime = 0;
            time = 0;

            switch (dtype)
            {
                case DamageType.BLUDGEON:
                case DamageType.SLASH:
                    damage = 5;
                    break;
                case DamageType.BULLET:
                    damage = 10;
                    break;
                case DamageType.AREA:
                    damage = 10;
                    break;
                case DamageType.POISON:
                    damage = 5;
                    time = 10;
                    damageInTime = 3;
                    break;
                case DamageType.DEATHLY:
                    damage = 5;
                    time = 5;
                    break;
                default:
                break;
            }
        }

        public Damage getDamage(){
            Damage dmg = new Damage(damType, multiply);
            return dmg;
        }

        public DamageType GetDamageType(){
            return damType;
        }

    }

}