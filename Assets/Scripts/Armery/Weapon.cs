using System.Collections;
using System.Collections.Generic;

namespace JMD.Armery
{
    public class Weapon
    {
      // armas definidas
      public enum WeaponType {
        PUNCH, KICK, KNIFE, POISONED, GUN, AK, DEATHLY, BOMB
      }
      public List<Damage> lDmg = new List<Damage>();
      public float range = 0f;
      public float fireTime = 1f;
      public WeaponType weaponType;

      // constructor del tipo
      public Weapon(WeaponType wType, float range = 5, float multiplidador = 1){
        weaponType = wType;
        initWeapon(weaponType);
      } 

      private void initWeapon (WeaponType wt) {
        Damage dm;
        range = 5;
        switch (wt)
        {
            case WeaponType.PUNCH:
              dm = new Damage(Damage.DamageType.BLUDGEON);
              lDmg.Add(dm);
              break;  
            case WeaponType.KICK:
              dm = new Damage(Damage.DamageType.BLUDGEON, 1.3f);
              lDmg.Add(dm);
              break;  
            case WeaponType.KNIFE:
              dm = new Damage(Damage.DamageType.SLASH, 3f);
              lDmg.Add(dm);
              break;
            case WeaponType.POISONED:
              dm = new Damage(Damage.DamageType.POISON);
              lDmg.Add(dm);
              dm = new Damage(Damage.DamageType.SLASH, 3f);
              lDmg.Add(dm);
              fireTime = 4;
              break;
            case WeaponType.BOMB:
              dm = new Damage(Damage.DamageType.AREA, 10f);
              range = 10;
              fireTime = 3f;
              lDmg.Add(dm);
              break;  
            case WeaponType.GUN:
              dm = new Damage(Damage.DamageType.BULLET);
              range = 20;
              fireTime = 1.2f;
              lDmg.Add(dm);
              break;  
            case WeaponType.AK:
              dm = new Damage(Damage.DamageType.BULLET, 3f);
              range = 40;
              fireTime = 0.2f;
              lDmg.Add(dm);
              break;
            case WeaponType.DEATHLY:
              dm = new Damage(Damage.DamageType.DEATHLY, 1f);
              range = 10;
              fireTime = 5f;
              lDmg.Add(dm); 
              break;  
            default:
            break;
        }
      }

    }   
}