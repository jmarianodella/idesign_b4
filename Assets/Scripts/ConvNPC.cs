﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class ConvNPC : MonoBehaviour
{

    public Camera playerCamera;
    public Camera npcCamera;
    public GameObject player;
    // Use this for initialization
    void Start()
    {
        playerCamera.enabled = true;
        npcCamera.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (Vector3.Distance(transform.position, player.transform.position) < 4)
            {
                playerCamera.enabled = false;
                npcCamera.enabled = true;
                player.transform.position = transform.position + transform.forward * 2;
                player.transform.LookAt(new Vector3(transform.position.x,
               player.transform.position.y, transform.position.z));
                FirstPersonController input = player.GetComponent<FirstPersonController>();
                input.enabled = false;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            playerCamera.enabled = true;
            npcCamera.enabled = false;
            FirstPersonController input = player.GetComponent<FirstPersonController>();
            input.enabled = true;
        }
    }
}
