﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConversationTree : MonoBehaviour
{

    [SerializeField] public List<Nodes> Trees = new List<Nodes>();

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
	
    [System.Serializable]
    public class Nodes
    {
        public List<ConvNode> nodes = new List<ConvNode>();
    }
    [System.Serializable]
    public class ConvNode
    {
        public string prompt;
        public string[] replies;
        public int[] nodeLinks;
    }
}
