﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JMD.Character
{
    [RequireComponent(typeof(Animator))]
    public class CharacterAnim : MonoBehaviour
    {
        private Animator anim;
        // Use this for initialization
        private const string WALK = "walk";
        private const string PUNCH = "punching";
        private const string POISON = "poison";
        private const string BOMB = "bomb";

        private const string TALK = "talk";

        private const string DEATHLY = "deathly";

        public bool isWalking;
        public bool isTalking;
        void Start()
        {
            anim = gameObject.GetComponent<Animator>();
            isWalking = anim.GetBool(WALK);
        }

        // Update is called once per frame
        void Update()
        {
            // anim.SetBool(WALK, isWalking);
        }
        public void walk()
        {
            isWalking = true;
             anim.SetBool(WALK, isWalking);
        }

        public void stay()
        {
            isWalking = false;
             anim.SetBool(WALK, isWalking);
        }

        public void punch()
        {
            anim.SetTrigger(PUNCH);
        }
        public void talk(bool t)
        {   
            isTalking = t;
            anim.SetBool(TALK, isTalking);
        }

        public void poison()
        {
            anim.SetTrigger(POISON);
        }
        public void bomb()
        {
            print("bomb trigger");
            anim.SetTrigger(BOMB);
        }
        public void deathly()
        {
            // anim.SetTrigger(POISON);
            anim.SetTrigger(DEATHLY);
        }

        public void sayHello(){
            anim.SetTrigger("hello");
        }
        public float getCurrentAnimTime()
        {
            return anim.GetCurrentAnimatorStateInfo(0).length;
        }
    }
}
