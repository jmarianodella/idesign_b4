﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JMD.Armery;

namespace JMD.Character
{
	[RequireComponent(typeof (CharacterMovement))]
	[RequireComponent(typeof (CharacterCombat))]
	[RequireComponent(typeof (CharacterHealth))]
    public class CharacterIA : MonoBehaviour
    {
		// distancia de detección
		public float detectionRange = 30f;
        
        public enum IAStatus {IDLE, APROACHING, ATTACKING, HOLDING_ATTACK};

        public IAStatus status = IAStatus.IDLE;
        // special attacks
        public int specialAttacks = 1000;
        public int poisonAttacks = 2;
       
        private string enemyTag = "Player";

        // Use this for initialization
        GameObject player;
		CharacterMovement chmov;
        CharacterCombat chcombat;
        CharacterHealth chhealth;



        

        void Start()
        {
            player = GameObject.FindGameObjectsWithTag(enemyTag)[0];
			chmov = gameObject.GetComponent<CharacterMovement>();
            chcombat = gameObject.GetComponent<CharacterCombat>();
            // puedo herir al objeto que busco
            chcombat.damagebleTag = enemyTag;
            chhealth = gameObject.GetComponent<CharacterHealth>();
        }

        // Update is called once per frame
        void Update()
        {
            // chooseWeapon();

            Weapon currentWeapon = chcombat.getCurrentWeapon();
            float weaponRange = currentWeapon.range;
            float playerDistance = Vector3.Distance(player.transform.position, transform.position);

            bool playerInWeaponRange = playerDistance < weaponRange;
            bool playerDetected  = playerDistance <= detectionRange;

            switch (status)
            {
                case IAStatus.IDLE:
                // check status
                    if (playerDetected){
                        status = IAStatus.APROACHING;
                        break;
                    }
                    chmov.stay();
                    status = IAStatus.IDLE;
                    break;
                case IAStatus.APROACHING:
                // check status
                    if (!playerDetected){
                        // nothing to do here
                        // go Home? de momento idle
                        status = IAStatus.IDLE;
                        break;
                    }else if (playerInWeaponRange){
                        // normal attack
                        status = IAStatus.ATTACKING;
                        break;
                    }

                    // nos acercamos al target
                    chmov.setTarget(player.transform.position);
                    break;

                case IAStatus.ATTACKING:
                    if (!chcombat.canFireWeapon()){
                        status = IAStatus.HOLDING_ATTACK;
                        break;
                    }
                    // check status
                    if (!playerInWeaponRange){
                        status = IAStatus.APROACHING;
                        break;
                    }

                    // si estamos atacando elegimos arma
                    chooseWeapon();
                    chcombat.fireWeapon();
                   
                    // orientar al objeto hacia el target
                    chmov.lookAt(player.transform.position);
                    break;
                case IAStatus.HOLDING_ATTACK:
                    if (chcombat.canFireWeapon()){
                        // si puede disparar lo pasamos a aproaching y de ahí ya hará
                        status = IAStatus.APROACHING;
                    }
                    break;
               
                default:
                    break;
            }
        }
        void chooseWeapon(){
            float chooseAttack = Random.Range(0,99);
            
            float percentHealth = (chhealth.health / chhealth.maxHealth);
            // choose weapon
             if (percentHealth <= 0.75f && poisonAttacks > 1){
                // vida baja y quedan ataques veneno
                // el arma especial será la última del arsenal
                chcombat.switchWeapon(chcombat.getWeapons().Count - 2);
                poisonAttacks--;
                return;
             }
             if (percentHealth <= 0.5f && specialAttacks > 0){
                // vida baja y quedan ataques especiales
                // el arma especial será la última del arsenal
                chcombat.switchWeapon(chcombat.getWeapons().Count - 1);
                specialAttacks--;
                return;
             }
             if (percentHealth <= 0.25f && poisonAttacks > 0){
                // vida baja y quedan ataques veneno
                // el arma especial será la última del arsenal
                chcombat.switchWeapon(chcombat.getWeapons().Count - 2);
                poisonAttacks--;
                return;
             }

            if (chooseAttack < 10){
                // ataque de área 10%
                chcombat.switchWeapon(1);
              
            }else {
                // elegir arma
                 chcombat.switchWeapon(0);
            }
        }
    }


}