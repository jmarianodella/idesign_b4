﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterNav : MonoBehaviour {

	NavMeshAgent navMeshAgent;
 	public Transform destination;
	 
// Use this for initialization
	void Start () {
		navMeshAgent = GetComponent<NavMeshAgent>();

	}
	
	// Update is called once per frame
	void Update () {
		navMeshAgent.SetDestination(destination.position);
	}
	public static Vector3 RandomNavSphere(Vector3 origin, GameObject player, float dist, int layermask)
 {
		Vector3 randDirection = Random.insideUnitSphere * dist;
		randDirection += origin;
		randDirection = new Vector3(randDirection.x, player.transform.position.y, randDirection.y);
		print(randDirection);
		NavMeshHit navHit;
		NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);
		return navHit.position;
}

}
