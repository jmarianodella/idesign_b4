﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JMD.Armery;

namespace JMD.Character
{

    public class CharacterHealth : MonoBehaviour
    {

        public float health = 100;
        public float maxHealth = 100;
        public bool isPlayer = false;
        public bool isDeath = false;
        public bool isPoisoned = false;
        public bool isDeathly = false;
        public float timeToDeath = 0;


        public List<Damage> lCurrentDamage = new List<Damage>();

        public float test = 0;

        // Use this for initialization
        void Start()
        {
            // health = maxHealth;
        }

        // Update is called once per frame
        void Update()
        {
           
            if (isDead())
            {
                if (isPlayer)
                {
                    health = maxHealth;
                    transform.position = new Vector3(0, 0, 0);
                }
                else
                {
                    Animator anim = GetComponent<Animator>();
                    if (anim)
                    {
                        if (isDeath)
                        {
                            anim.SetTrigger("die");
                            isDeath = false;
                        }
                        GameObject.Destroy(gameObject, 3);
                    }
                    else
                    {
                        GameObject.Destroy(gameObject);
                    }

                }
            }else{
                 doDamageInTime();
            }

            if (isDeathly){
                timeToDeath -= Time.deltaTime;
                if (timeToDeath <= 0){
                    isDeath = true;
                }
            }

            inputManagement();

        }
        void OnTriggerEnter(Collider collider)
        {
            print("entrando en colisión");
            if(collider.tag=="Health"){
                isPoisoned = false;
                Debug.Log("Estoy chocando con un trigger");
            }
        }
        void inputManagement(){
            if (Input.GetKeyDown(KeyCode.E)){
                isDeathly = false;
            }
        }

        private void doDamage(float dmg)
        {
            health -= dmg;
            if (isDead())
            {
                isDeath = true;
            }
        }

        public void doDamage(List<Damage> ldmg)
        {
            foreach (var dmg in ldmg)
            {
                switch (dmg.GetDamageType())
                {
                    case Damage.DamageType.POISON:
                        isPoisoned = true;
                        break;
                    case Damage.DamageType.DEATHLY:
                        isDeathly = true;
                        timeToDeath = dmg.getDamage().damage;
                        // el ataque no hace daño
                        dmg.damage = 0;
                        break;
                    default:
                        break;
                }
                doDamage(dmg.damage);
                // Debug.Log("dmg.time " + dmg.time);
                if (dmg.time > 0)
                {
                    Debug.Log("damage in time");
                    lCurrentDamage.Add(dmg.getDamage());
                }
            }
        }


        private bool isDead()
        {
            return health <= 0;
        }

        private void doDamageInTime()
        {
            for (int i = lCurrentDamage.Count - 1; i >= 0 ; i--)
            {
                if (lCurrentDamage[i].time > 0){
                    lCurrentDamage[i].time -= Time.deltaTime;
                    switch (lCurrentDamage[i].GetDamageType())
                    {
                        case Damage.DamageType.POISON:
                            if (isPoisoned){
                                doDamage(lCurrentDamage[i].damageInTime * Time.deltaTime);
                            }else{
                                lCurrentDamage.Remove(lCurrentDamage[i]);
                            }
                            break;
                        default:
                            doDamage(lCurrentDamage[i].damageInTime * Time.deltaTime);
                            break;  
                    }
    
                }else{
                    lCurrentDamage.Remove(lCurrentDamage[i]);
                }
            }
            test = lCurrentDamage.Count;
        }
    }

}