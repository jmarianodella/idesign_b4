﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JMD.Character
{

    public class CharacterMovement : MonoBehaviour
    {
        public enum MovSTATUS { IDLE, MOVEMENT, ROTATION };
        public MovSTATUS status = MovSTATUS.IDLE;
        // Use this for initialization
        public Vector3 target;
        // distancia a la cual se considera que está en el destino
        public float cerca = 2;

        public float speed = 1;
        public float speedRotation = 1;
        // animacion de personaje
        CharacterAnim anim;
        void Start()
        {
            anim = gameObject.GetComponent<CharacterAnim>();
            status = MovSTATUS.IDLE;

        }

        // Update is called once per frame
        void Update()
        {
            Vector3 direccion;
            Quaternion rot;
            switch (status)
            {
                case MovSTATUS.IDLE:
                    anim.stay();
                    break;
                case MovSTATUS.MOVEMENT:

                    if ((Vector3.Distance(target, transform.position) >= cerca))
                    {
                        anim.walk();
                        direccion = (target - transform.position);
                        rot = Quaternion.LookRotation(direccion, Vector3.up);
                        transform.rotation = Quaternion.Slerp(transform.rotation, rot, speedRotation * Time.deltaTime);
                        transform.Translate(transform.worldToLocalMatrix.MultiplyVector(transform.forward)
                            * Time.deltaTime * speed);
                    }
                    else
                    {
                        anim.stay();
                    }
                    break;
                case MovSTATUS.ROTATION:
                    anim.walk();
                    direccion = (target - transform.position);
                    direccion.y = transform.position.y;
                    rot = Quaternion.LookRotation(direccion, Vector3.up);
                    transform.rotation = Quaternion.Slerp(transform.rotation, rot, speedRotation * Time.deltaTime);
                    break;
                default:
                
                    break;
            }
        }

        public void setTarget(Vector3 t)
        {
            status = MovSTATUS.MOVEMENT;
            target = t;
        }

        public void lookAt(Vector3 t)
        {
            status = MovSTATUS.ROTATION;
            target = t;
        }

        public void stay()
        {
            status = MovSTATUS.IDLE;
        }
    }

}