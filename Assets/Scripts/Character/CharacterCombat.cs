﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JMD.Armery;

namespace JMD.Character
{

    public class CharacterCombat : MonoBehaviour
    {

        public enum CombatStatus {IDLE, PREPARING_WEAPON, FIRING_WEAPON, RELOADING}
        public CombatStatus status = CombatStatus.IDLE;

        public List<Weapon.WeaponType> weapons = new List<Weapon.WeaponType>();
        public Weapon.WeaponType currentWeaponType;
        private int currentWeapon = 0;
        public string cambiarArma = "presionar PageUp";
        public string damagebleTag = "Respawn";
        public GameObject area;

        // targetInfo
        RaycastHit targetHit;
        // weapons list
        List<Weapon> lWeapon = new List<Weapon>();
        // time to wait before shoot again
        private float waitToFire = 0;
        // animacion de personaje
        CharacterAnim anim;
        // tiempo de la animación antes de realizar el ataque
        float animationTime;
        // impacted objects
        List<GameObject> target;
        void Start()
        {

            animationTime = -1;
            anim = gameObject.GetComponent<CharacterAnim>();
            foreach (Weapon.WeaponType weapontType in weapons)
            {
                Weapon weapon = new Weapon(weapontType);
                lWeapon.Add(weapon);
            }
            currentWeaponType = weapons[0];

        }


        // Update is called once per frame
        void Update()
        {
            switch (status)
            {
                case CombatStatus.PREPARING_WEAPON:
                    if (animationTime > 0){
                        animationTime -= Time.deltaTime;
                    }else{
                        status = CombatStatus.FIRING_WEAPON;
                    }
                    break;
                case CombatStatus.FIRING_WEAPON:
                    print("Boom!!");
                    shootWeapon();
                    status = CombatStatus.RELOADING;
                    break;
                case CombatStatus.RELOADING:
                    if (waitToFire > 0){
                        waitToFire -= Time.deltaTime;
                    }else{
                        status = CombatStatus.IDLE;
                    }
                    break;
                case CombatStatus.IDLE:
                
                    if (Input.GetButtonDown("Fire1") && damagebleTag == "Respawn")
                    {
                        fireWeapon();
                       
                        break;
                    } 
                    if (Input.GetKeyDown(KeyCode.PageUp))
                    {
                        Debug.Log("PageUp key was pressed. " + weapons.Count);
                        currentWeapon = (currentWeapon + 1) % weapons.Count;
                        currentWeaponType = weapons[currentWeapon];
                    }
                    break;
                default: 
                    break;
            }
            // updating fireTime
           
            


        }

        // LIBRERÍAS PÚBLICAS
        public Weapon getCurrentWeapon()
        {
            return lWeapon[currentWeapon];
        }

        public List<Weapon> getWeapons()
        {
            return lWeapon;
        }
        public void switchWeapon(int index)
        {
            print ("new weapon " + index);
            if (index < lWeapon.Count)
            {
                currentWeapon = index;
                waitToFire = 0;
                print("new weapon " + getCurrentWeapon().weaponType);
            }
        }
        public bool canFireWeapon()
        {
            return status == CombatStatus.IDLE;
        }

        public void fireWeapon()
        {
            // print("disparando fire1");
            if (status == CombatStatus.IDLE){
                prepareWeapon();
                status = CombatStatus.PREPARING_WEAPON;
            }

           
        }


        // LIBRERÍAS PRIVADAS
        private void prepareWeapon(){
            if (anim)
            {
                animationTime = anim.getCurrentAnimTime()/2f ;
                // animationTime = 0.1f ;
                print("waiting time" + animationTime);
                switch (getCurrentWeapon().weaponType)
                {
                    case Weapon.WeaponType.BOMB:
                       
                        // animación del personaje
                        anim.bomb();
                        break;
                    case Weapon.WeaponType.POISONED:
                       
                        // animación del personaje
                        anim.poison();
                        break;
                    case Weapon.WeaponType.DEATHLY:
                       
                        // animación del personaje
                        anim.deathly();
                        break;
                    
                    
                    default:
                        anim.punch();
                        break;
                }
            }
        }
        void mainShoot(GameObject item)
        {
            // print("impacto en " + targetHit.collider.gameObject.tag);
            if (item.tag == damagebleTag)
            {
                print("enemigo impactado");
                // si el daño es POISON el daño depende de lo cerca que esté del origen
                foreach (Damage dmg in lWeapon[currentWeapon].lDmg)
                {
                    if (dmg.GetDamageType() == Damage.DamageType.AREA){
                         Vector3 pos = transform.position;
                         Vector3 itemPos = item.transform.position;
                         float distance = Vector3.Distance(pos, itemPos);
                         dmg.damage = dmg.damage - dmg.damage * (distance / lWeapon[currentWeapon].range);
                    }
                } 
                CharacterHealth enemy = item.GetComponent<CharacterHealth>();
                enemy.doDamage(lWeapon[currentWeapon].lDmg);

            }
        }

        void shootWeapon(){
             // actualizamos tiempo de espera para el siguiente disparo del arma
             // *2 para debug
            waitToFire = lWeapon[currentWeapon].fireTime * 2;
            // miramos si ha impactado en alguien
            hitTarget(lWeapon[currentWeapon].range);
            if (lWeapon[currentWeapon].weaponType == Weapon.WeaponType.BOMB){
                // generamos animación de área
                generaArea(Weapon.WeaponType.POISONED);
            }
            foreach (GameObject item in target)
            {
                // calculamos si hay daño
                mainShoot(item);
            }
        }
        /**
         * Detecta si hay impacto dentro del alcance, en caso afirmativo 
         * almacena el tag del target y su distancia
         */
        bool hitTarget(float range)
        {
            target = new List<GameObject>();
            switch (getCurrentWeapon().weaponType)
            {
                case Weapon.WeaponType.BOMB:
                    return hitAreaCast(range);
                default:
                    return hitRayCast(range);
            }
        }

        /**
        * lanza ataque de raycast
        */
        bool hitRayCast(float range){
             RaycastHit hit;
             
            Debug.DrawRay(
                transform.position,
                transform.TransformDirection(Vector3.forward) * range,
                Color.yellow);

            if (Physics.Raycast(
                    new Vector3(transform.position.x, transform.position.y + 1, transform.position.z),
                    transform.TransformDirection(Vector3.forward) * 10,
                    out hit,
                    range))
            {

                targetHit = hit;
                target.Add(targetHit.collider.gameObject);

                // print("Te detecto");
                return true;
            }
            return false;
        }

        /**
         * lanza ataque de area
         */
         bool hitAreaCast(float radius){
            GameObject[] damagebleTargets =  GameObject.FindGameObjectsWithTag(damagebleTag);
            Vector3 pos = transform.position;

            foreach (GameObject item in damagebleTargets)
            {  
                Vector3 itemPos = item.transform.position;
                float distance = Vector3.Distance(pos, itemPos);
                if (distance <= radius){
                    target.Add(item);
                }    
            }
             return false;
         }
         /**
          * genera animación del ataque de area
          */
          void generaArea(Weapon.WeaponType type){
              Vector3 pos = transform.position;
              pos.y = 0;
              // GameObject area = Resources.Load<GameObject>("area/areamagic");
              GameObject clone = Instantiate(area, pos, transform.rotation);
              Destroy(clone, 2);
          }
    }

}