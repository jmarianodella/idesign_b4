﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

namespace JMD.Character
{

    public class CharacterTalk : MonoBehaviour
    {

        public enum TalkStatus { IDLE, TALKING }

        public int actualNode;
        public GameObject canvas;
        public GameObject conversationTree;
        public ConversationTree convTreeScript;
        public float detectionDistance = 4;
        public Camera npcCamera;
        public GameObject player;
        public Camera playerCamera;
        public TalkStatus status;

        // entorno privado
        // animacion de personaje
        CharacterAnim anim;
        CharacterMovement chmov;
        CharacterBag chbag;
        // Use this for initialization
        void Start()
        {
            status = TalkStatus.IDLE;
            anim = gameObject.GetComponent<CharacterAnim>();
            chmov = gameObject.GetComponent<CharacterMovement>();
            chbag = player.GetComponent<CharacterBag>();
            if (anim== null){
                print("no hay animacion");
            }else{
                print("pues sí que hay");
            }

            conversationTree = GameObject.Find("ConversationTree");
          
            convTreeScript = conversationTree.GetComponent<ConversationTree>();
            canvas = GameObject.Find("Canvas");

        }

        // Update is called once per frame
        void Update()
        {
            switch (status)
            {
                case TalkStatus.IDLE:
                    float distancePlayer = Vector3.Distance(transform.position, player.transform.position);
                    if (distancePlayer < detectionDistance && chmov)
                    {
                        chmov.lookAt(player.transform.position);
                    } else{
                        chmov.lookAt(new Vector3(0,0,10));

                    }
                        
                    if (Input.GetKeyDown(KeyCode.E) && inFOV() )
                    {
                        if (distancePlayer < detectionDistance)
                        {
                            if (chbag.has("mojo")){
                                initTalking();
                            }else{
                                anim.sayHello();
                            }
                        }
                    }
                    break;
                case TalkStatus.TALKING:
                    if (Input.GetKeyDown(KeyCode.Escape))
                    {
                        finishTalking();
                    }
                    break;
                default:
                    break;
            }


        }
        private void teveo(){
            var heading = player.transform.position.normalized - transform.position.normalized;
            var dot = Vector3.Dot(heading, transform.forward);
            print(dot);
        }
        // in field of view
        private bool inFOV(){
            var heading = player.transform.position.normalized - transform.position.normalized;
            var dot = Vector3.Dot(heading, transform.forward);
            print(dot);
            return dot > 0.8;
        }
        public void getAnswer(Button button)
        {
            switch (button.name)
            {
                case "TextA":
                    populateFields(0);
                    break;
                case "TextB":
                    populateFields(1);
                    break;
                case "TextC":
                    populateFields(2);
                    break;
                default:
                    break;
            }

        }

        public void populateFields(int v)
        {
            print("iniciando node " + v + ": " + convTreeScript.Trees[0].nodes[actualNode].prompt);
            actualNode = convTreeScript.Trees[0].nodes[actualNode].nodeLinks[v];
            if (actualNode < 0)
            {
                finishTalking();
                return;
            }
            updateButtonsText();
        }

        private void updateButtonsText(){
            print("asdf");
            int numReplies = convTreeScript.Trees[0].nodes[actualNode].replies.Length;
            // // texto principal
            canvas.transform.Find("Panel").Find("Title").GetComponent<Text>().text = 
                convTreeScript.Trees[0].nodes[actualNode].prompt;
            // if (numReplies > 0)
            // {
            //     canvas.transform.Find("Panel").Find("TextA").GetChild(0).GetComponent<Text>().text =
            //         convTreeScript.Trees[0].nodes[actualNode].replies[0];
            // }

            for (int indexReply = 0; indexReply < 3; indexReply++)
            {
                string currentButton = "TextA";
                switch (indexReply)
                {
                    case 0:
                        currentButton = "TextA";
                        break;
                    case 1:
                        currentButton = "TextB";
                        break;
                    case 2:
                        currentButton = "TextC";
                        break;
                    default:
                        break;
                }
                try
                {
                    if (indexReply < numReplies){
                        canvas.transform.Find("Panel").Find(currentButton).GetChild(0).GetComponent<Text>().text =
                            convTreeScript.Trees[0].nodes[actualNode].replies[indexReply];
                        canvas.transform.Find("Panel").Find(currentButton).GetComponent<Button>().interactable = true;
                    }else{
                        canvas.transform.Find("Panel").Find(currentButton).GetChild(0).GetComponent<Text>().text ="";
                        canvas.transform.Find("Panel").Find(currentButton).GetComponent<Button>().interactable = false;
                    }
                    
                }
                catch (System.Exception)
                {
                    print("error");
                    
                }
            }
        }

        private void finishTalking()
        {
            status = TalkStatus.IDLE;

            playerCamera.enabled = true;
            npcCamera.enabled = false;
            FirstPersonController input = player.GetComponent<FirstPersonController>();
            input.enabled = true;

            if (anim) anim.talk(false);
            showCanvas(false);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            // si la opcion elegida es 3 no vuelve a hablar
            if (actualNode == -2) {
                this.enabled = false;
            }else{
                print(actualNode);
            }
        }

        private void initTalking()
        {
            FirstPersonController input = player.GetComponent<FirstPersonController>();
            actualNode = 0;
            status = TalkStatus.TALKING;

            playerCamera.enabled = false;
            npcCamera.enabled = true;
            input.enabled = false;

            player.transform.position = transform.position + transform.forward * 2;
            player.transform.LookAt(new Vector3(transform.position.x,
            player.transform.position.y, transform.position.z));

            if (anim) anim.talk(true);

            updateButtonsText();
            showCanvas(true);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        private void showCanvas(bool shouldShow)
        {
            CanvasGroup cv = canvas.GetComponent<CanvasGroup>();
            if (shouldShow)
            {
                cv.alpha = 1.0f;
                cv.blocksRaycasts = true;
            }
            else
            {
                cv.alpha = 0.0f;
                cv.blocksRaycasts = false;
            }
        }
    }
}