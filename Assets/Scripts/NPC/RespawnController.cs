﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnController : MonoBehaviour
{

    public int numToSpawn = 3;
    public float timeToRespawn = 2;
    // list of prefabs to spawn
    private float contador;
    public List<GameObject> npcList;
    public float minDistance = 2;
    public float maxDistance = 5;

    // Use this for initialization
    void Start()
    {
        // numToSpawn = 3;
        // timeToRespawn = 2;
        print("start");
    }
    // Update is called once per frame
    void Update()
    {
    }

    private void FixedUpdate()
    {
       int respawns = transform.childCount;
        if (respawns < numToSpawn)
        {
            //spawneamos
            contador = contador + Time.deltaTime;
            if (contador >= timeToRespawn)
            {
                while (!respawnCreature())
                {
                    print("generando personaje en posición prohibida");
                }
                contador = 0;
            }
        }
    }
    bool respawnCreature()
    {
        Vector2 newPosition = Random.insideUnitCircle * maxDistance;
        // si el punto está dentro de la distancia mínima no generamos personaje
        if (newPosition.magnitude < minDistance)
        {
            return false;
        }
        int numberRandom = Random.Range(0, npcList.Count);
        GameObject clone = Instantiate(npcList[numberRandom],
            new Vector3(transform.position.x + newPosition.x, 0, transform.position.z + newPosition.y),
            Quaternion.Euler(0, Random.Range(0, 360),0));
        clone.transform.SetParent(transform);
        return true;
    }

}
