# Character
url
https://gitlab.com/jmarianodella/idesign_b4.git

crec que m'he complicat un poc sa vida però he aprofitat per provar de separar els elements que formen un Character (tant player com NPC) per separat i així emproar el projecte final.

Scripts/Character/:
- CharacterIA: es el que pren les decissions: on anar, quina arma triar
- CharacterAnim: gestiona els paràmetres de les animacions associades al Character.
- CharacterCombat: du la gestió de combat, si un arma es pot disparar, si a impactat a l'enemic corresponent. També ordena a CharacterAnim l'animació corresponent.
- CharacterMovement: gestiona el desplaçament i la rotació del personatge. També ordena a CharacterAnim l'animació corresponent.
- CharacterHealth: gestiona la vida del personatge. Quan reb impacte aplica tots els tipus de Damage associats a l'arma que l'ha ferit. Segons el tipus de Damage altera l'estat: Poisoned / Deathly. En cas de passar per un objecte de "Curació" elimina l'estat Poisoned. Detecta l'entrada de teclat "E" per eliminar l'estat Deathly.

Per les armes ho he separat en:
Scripts/Armery:
- Weapon: Genera les armes segons el tipus. Un tipus associat pot tener més d'un tipus de Damage.
- Damage: Mal que fa a un Character, pot tenir una quantitat de mal per fer de manera inmediata (Damage.damage) o Mal durant un temps (Damage.damageInTime i Damage.time)

# Tema 15: Interacción con NPCs

Escena: B4Tema15
Ej.2 Campo de visión: función CharacterTalk.teveo()
Ej.3 Para dejar pasar seleccionar la opcion 3
Ej.4 Objeto a llevar:  En CharacterBag, el elemento 0 llamarlo "mojo"